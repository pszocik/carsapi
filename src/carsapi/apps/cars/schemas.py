from ninja import ModelSchema, Schema

from carsapi.apps.cars.models import Car


class CarCreateSchema(ModelSchema):
    class Config:
        model = Car
        model_fields = ["make", "model"]


class RatingCreateSchema(Schema):
    car_id: int
    value: int


class CarRetrieveSchema(ModelSchema):
    avg_rating: float

    class Config:
        model = Car
        model_fields = ["id", "make", "model"]


class CarPopularRetrieveSchema(ModelSchema):
    rates_number: float

    class Config:
        model = Car
        model_fields = ["id", "make", "model"]
