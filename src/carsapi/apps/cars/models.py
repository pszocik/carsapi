from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Sum

from carsapi.apps.core.fields import LowerCaseCharField


class Car(models.Model):
    make = LowerCaseCharField(blank=False, null=False, max_length=512)
    model = LowerCaseCharField(blank=False, null=False, max_length=512)

    class Meta:
        unique_together = ("make", "model")

    def __str__(self):
        return f"{self.make} {self.model}"

    @property
    def avg_rating(self):
        ratings_sum = self.ratings.aggregate(Sum("value"))["value__sum"]
        ratings_count = self.ratings.count()
        if ratings_sum is None:
            return 0
        return ratings_sum / ratings_count


class Rating(models.Model):
    car = models.ForeignKey(to=Car, related_name="ratings", on_delete=models.CASCADE)
    value = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])

    def __str__(self):
        return f"{self.car}: {self.value}"
