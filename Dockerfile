FROM python:3.8 as base

ENV PYTHONUNBUFFERED 1
ENV DEBUG=False
ENV SECRET_KEY=super_secret_key

WORKDIR /code
COPY pyproject.toml poetry.lock /code/

RUN apt-get update
RUN pip install --upgrade -U pip && pip install -U poetry==1.1.8
RUN poetry config virtualenvs.create false --local && poetry install --no-dev

COPY ./src /code/

FROM base as dev
WORKDIR /code
CMD python manage.py runserver 0.0.0.0:8000

FROM base as production
CMD exec gunicorn carsapi.wsgi:application --bind 0.0.0.0:$PORT
