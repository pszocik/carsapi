from typing import List

from django.db import IntegrityError
from django.db.models import Count
from django.shortcuts import get_object_or_404
from ninja import Router
from ninja.errors import HttpError

from carsapi.apps.cars.vpic_api import is_in_vpic_api
from carsapi.apps.cars.models import Car, Rating
from carsapi.apps.cars.schemas import (
    CarCreateSchema,
    CarRetrieveSchema,
    RatingCreateSchema,
    CarPopularRetrieveSchema,
)

router = Router()


@router.get("/cars", response=List[CarRetrieveSchema])
def cars_list(request):
    qs = Car.objects.all()
    return qs


@router.post("/cars", response={201: int, 404: str})
def cars_create(request, payload: CarCreateSchema):
    if not is_in_vpic_api(payload):
        raise HttpError(
            404,
            "Car with given make and model not found in Vehicle API. Please visit https://vpic.nhtsa.dot.gov/api/ and "
            "verify your request.",
        )
    try:
        car = Car.objects.create(**payload.dict())
    except IntegrityError as e:
        return 404, {"detail": e.args}
    return 201, car.id


@router.delete("/cars/{car_id}")
def cars_delete(request, car_id: int):
    car = get_object_or_404(Car, id=car_id)
    car.delete()
    return {"success": True}


@router.post("/rate", response={201: int})
def cars_rate(request, payload: RatingCreateSchema):
    car = get_object_or_404(Car, id=payload.car_id)
    rating = Rating.objects.create(car=car, value=payload.value)
    return 201, rating.id


@router.get("/popular", response=List[CarPopularRetrieveSchema])
def cars_popular(request):
    qs = (
        Car.objects.all()
        .annotate(rates_number=Count("ratings"))
        .order_by("-rates_number")
    )
    return qs
