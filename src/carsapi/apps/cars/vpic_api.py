import requests


def is_in_vpic_api(payload):
    make = payload.make
    model = payload.model
    response = requests.get(
        f"https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformake/{make}?format=json"
    )
    response_make_list = response.json()["Results"]
    for response_make_model in response_make_list:
        if response_make_model["Model_Name"].lower() == model.lower():
            return True
    return False
