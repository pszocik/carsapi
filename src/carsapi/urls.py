from django.contrib import admin
from django.urls import path
from django.views.generic import RedirectView

from carsapi.api import api

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", api.urls),
    path("", RedirectView.as_view(url="api/docs"), name="redirect_to_docs"),
]
