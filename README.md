## Table of contents
* [General info](#general-info)
* [Setup](#setup)

## General info
This project is simple cars API.
	
## Setup
Clone this repo and then run:

```
Create .env file and add DEBUG(True/False) and SECRET_KEY
(some secret value) then run:
$ docker-compose -f docker-compose.dev.yml up -d
$ docker exec <app_container_name> python manage.py migrate
```
After that visit [http://0.0.0.0:8000/api/docs](http://0.0.0.0:8000/api/docs) to check available endpoints