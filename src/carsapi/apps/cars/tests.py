import pytest
from ninja.testing.client import TestClient

from carsapi.apps.cars.api import router
from carsapi.apps.cars.models import Car


class TestCars:
    @pytest.fixture
    def ninja_client(self):
        return TestClient(router_or_app=router)

    @pytest.fixture
    def car(self):
        return Car.objects.create(model="test", make="test")

    def test_cars_list(self, ninja_client: TestClient):
        response = ninja_client.get("/cars")
        assert response.status_code == 200

    def test_cars_create_should_pass_with_proper_data(self, ninja_client: TestClient):
        response = ninja_client.post("/cars", json={"model": "a4", "make": "audi"})
        assert response.status_code == 201

    def test_cars_create_should_fail_with_wrong_data(self, ninja_client: TestClient):
        response = ninja_client.post("/cars", json={})
        assert response.status_code == 422

    def test_cars_delete_should_pass_if_car_exists(
        self, ninja_client: TestClient, car: Car
    ):
        response = ninja_client.delete(f"/cars/{car.id}")
        assert response.status_code == 200

    def test_cars_delete_should_ail_if_car_doesnt_exists(
        self, ninja_client: TestClient
    ):
        response = ninja_client.delete("/cars/999")
        assert response.status_code == 404

    def test_cars_rate_should_pass_if_car_exists(
        self, ninja_client: TestClient, car: Car
    ):
        response = ninja_client.post("/rate", json={"car_id": car.id, "value": 2})
        assert response.status_code == 201

    def test_cars_rate_should_fail_if_car_doesnt_exists(self, ninja_client: TestClient):
        response = ninja_client.post("/rate", json={"car_id": 999, "value": 2})
        assert response.status_code == 404

    def test_cars_popular(self, ninja_client: TestClient):
        response = ninja_client.get("/popular")
        assert response.status_code == 200
