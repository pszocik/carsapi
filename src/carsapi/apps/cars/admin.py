from django.contrib import admin

from carsapi.apps.cars.models import Car, Rating


class RatingInline(admin.TabularInline):
    model = Rating


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    inlines = [
        RatingInline,
    ]


admin.site.register(Rating)
