from ninja import NinjaAPI
from carsapi.apps.cars.api import router as cars_router


api = NinjaAPI(title="carsapi")

api.add_router("/", cars_router)
